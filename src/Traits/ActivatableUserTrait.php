<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Traits;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait ActivatableUserTrait
{
    /**
     * @Assert\NotNull
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected bool $activated = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected ?DateTimeInterface $updatedTokenTime;

    /**
     * @ORM\Column(nullable=true, unique=true)
     */
    protected ?string $activateToken;

    public function isActivated(): bool
    {
        return $this->activated;
    }

    public function setActivated(bool $activated): self
    {
        $this->activated = $activated;

        return $this;
    }

    public function getUpdatedTokenTime(): ?DateTimeInterface
    {
        return $this->updatedTokenTime;
    }

    public function setUpdatedTokenTime(?DateTimeInterface $updatedTokenTime): self
    {
        $this->updatedTokenTime = $updatedTokenTime;

        return $this;
    }

    public function getActivateToken(): ?string
    {
        return $this->activateToken;
    }

    public function setActivateToken(?string $activateToken): self
    {
        $this->activateToken = $activateToken;

        return $this;
    }
}
