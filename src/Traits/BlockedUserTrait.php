<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait BlockedUserTrait
{
    /**
     * @Assert\NotNull
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected bool $blocked = false;

    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    public function setBlocked(bool $blocked): self
    {
        $this->blocked = $blocked;

        return $this;
    }
}
