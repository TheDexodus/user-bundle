<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Listener;

use Tunet\UserBundle\Model\PlainPasswordAccessInterface;
use Tunet\UserBundle\Service\PasswordUpdaterInterface;

class UpdatePasswordListener
{
    protected PasswordUpdaterInterface $passwordUpdater;

    public function __construct(PasswordUpdaterInterface $passwordUpdater)
    {
        $this->passwordUpdater = $passwordUpdater;
    }

    public function prePersist(PlainPasswordAccessInterface $user): void
    {
        $this->passwordUpdater->hashPassword($user);
    }

    public function preUpdate(PlainPasswordAccessInterface $user): void
    {
        $this->passwordUpdater->hashPassword($user);
    }

    public function preFlush(PlainPasswordAccessInterface $user): void
    {
        $this->passwordUpdater->hashPassword($user);
    }
}
