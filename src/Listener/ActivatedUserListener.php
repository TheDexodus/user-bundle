<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Listener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Tunet\UserBundle\Exception\BlockedUserException;
use Tunet\UserBundle\Exception\NotActivatedUserException;
use Tunet\UserBundle\Model\ActivatableUserInterface;
use Tunet\UserBundle\Model\BlockedUserInterface;

class ActivatedUserListener
{
    protected TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest(): void
    {
        $token = $this->tokenStorage->getToken();

        if (!$token instanceof TokenInterface) {
            return;
        }

        $user = $token->getUser();

        if ($user instanceof BlockedUserInterface && $user->isBlocked()) {
            throw new BlockedUserException('User is blocked.');
        }

        if ($user instanceof ActivatableUserInterface && !$user->isActivated()) {
            throw new NotActivatedUserException('User not activated.');
        }
    }
}
