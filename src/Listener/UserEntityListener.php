<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Listener;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Tunet\UserBundle\Model\PlainPasswordAccessInterface;

/** @todo: check performance */
class UserEntityListener
{
    protected string $userClass;
    protected string $userRepositoryClass;

    public function __construct(string $userClass, string $userRepositoryClass)
    {
        $this->userClass = $userClass;
        $this->userRepositoryClass = $userRepositoryClass;
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $args): void
    {
        $metadata = $args->getClassMetadata();

        if ($metadata->rootEntityName !== $this->userClass) {
            return;
        }

        $this->configureUpdatePasswordListener($metadata);
        $this->configureUserRepository($metadata);
    }

    protected function configureUpdatePasswordListener(ClassMetadata $metadata): void
    {
        if (!in_array(PlainPasswordAccessInterface::class, class_implements($this->userClass), true)) {
            return;
        }

        if (!$this->hasEntityListener(Events::prePersist, $metadata)) {
            $metadata->entityListeners[Events::prePersist][] = [
                'class'  => UpdatePasswordListener::class,
                'method' => Events::prePersist,
            ];
        }

        if (!$this->hasEntityListener(Events::preUpdate, $metadata)) {
            $metadata->entityListeners[Events::preUpdate][] = [
                'class'  => UpdatePasswordListener::class,
                'method' => Events::preUpdate,
            ];
        }

        if (!$this->hasEntityListener(Events::preFlush, $metadata)) {
            $metadata->entityListeners[Events::preFlush][] = [
                'class'  => UpdatePasswordListener::class,
                'method' => Events::preFlush,
            ];
        }
    }

    protected function configureUserRepository(ClassMetadata $metadata): void
    {
        if ($metadata->customRepositoryClassName) {
            return;
        }

        $metadata->setCustomRepositoryClass($this->userRepositoryClass);
    }

    protected function hasEntityListener(string $eventName, ClassMetadata $metadata): bool
    {
        foreach ($metadata->entityListeners as $event => $listenersData) {
            if ($event === $eventName) {
                foreach ($listenersData as $listenerData) {
                    if (UpdatePasswordListener::class === $listenerData['class']
                        && $eventName === $listenerData['method']
                    ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
