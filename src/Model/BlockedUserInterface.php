<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Model;

interface BlockedUserInterface
{
    public function isBlocked(): bool;

    public function setBlocked(bool $blocked): self;
}
