<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Model;

interface ActivatableUserInterface
{
    public function isActivated(): bool;
}
