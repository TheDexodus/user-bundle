<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Model;

interface PlainPasswordAccessInterface
{
    public function getPlainPassword(): ?string;

    public function setPlainPassword(?string $plainPassword): self;

    public function setPassword(string $password): self;

    public function eraseCredentials(): void;
}
