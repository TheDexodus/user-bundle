<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;
use Tunet\UserBundle\Model\ActivatableUserInterface;

/**
 * @method UserInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserInterface|null findOneByEmail(string $email)
 * @method UserInterface[]    findAll()
 * @method UserInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $userClass)
    {
        parent::__construct($registry, $userClass);
    }

    public function save(UserInterface $user): void
    {
        $this->_em->persist($user);
        $this->_em->flush($user);
    }

    /** @todo: */
    public function getUserByActivateTokenAndLifetime(string $token, int $lifetime): ActivatableUserInterface
    {
        //        $queryBuilder = $this->createQueryBuilder('User');
        //        $queryBuilder
        //            ->andWhere('User.activateToken = :activateToken')
        //            ->andWhere('User.updatedTokenTime >= :updatedTokenTime')
        //            ->setParameter('activateToken', $token)
        //            ->setParameter('updatedTokenTime', new DateTime(sprintf('now - %d hours', $lifetime)))
        //        ;
        //
        //        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
