<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Tunet\UserBundle\Model\ActivatableUserInterface;

interface UserRepositoryInterface
{
    public function save(UserInterface $user): void;

    public function getUserByActivateTokenAndLifetime(string $token, int $lifetime): ActivatableUserInterface;
}
