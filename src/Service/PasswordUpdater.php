<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Service;

use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Tunet\UserBundle\Model\PlainPasswordAccessInterface;

class PasswordUpdater implements PasswordUpdaterInterface
{
    protected EncoderFactoryInterface $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function hashPassword(PlainPasswordAccessInterface $user): void
    {
        $password = $user->getPlainPassword();

        if (null === $password || !$password) {
            return;
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($password, $user->getSalt()));
        $user->eraseCredentials();
    }
}
