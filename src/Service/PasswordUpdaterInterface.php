<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Service;

use Tunet\UserBundle\Model\PlainPasswordAccessInterface;

interface PasswordUpdaterInterface
{
    public function hashPassword(PlainPasswordAccessInterface $user): void;
}
