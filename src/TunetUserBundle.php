<?php

declare(strict_types=1);

namespace Tunet\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class TunetUserBundle extends Bundle
{
}
