<?php

declare(strict_types=1);

namespace Tunet\UserBundle\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class NotActivatedUserException extends AccessDeniedHttpException
{
}
