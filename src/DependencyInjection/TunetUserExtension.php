<?php

declare(strict_types=1);

namespace Tunet\UserBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Tunet\UserBundle\Repository\UserRepository;
use Tunet\UserBundle\Repository\UserRepositoryInterface;

final class TunetUserExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $config = $this->processConfiguration(new Configuration(), $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');
        $loader->load('listeners.yaml');

        $this->setConfiguration($config, $container);
        $this->configureUserRepository($container);
    }

    private function setConfiguration(array $config, ContainerBuilder $container): void
    {
        $container->setParameter('tunet_user.user_class', $config['user_class']);
        $container->setParameter('tunet_user.user_repository', $config['user_repository']);
        $container->setParameter('tunet_user.login_route', $config['login_route']);
        $container->setParameter('tunet_user.home_route', $config['home_route']);
    }

    private function configureUserRepository(ContainerBuilder $container): void
    {
        $userRepositoryClass = $container->getParameter('tunet_user.user_repository');

        if (!in_array(UserRepositoryInterface::class, class_implements($userRepositoryClass), true)) {
            throw new InvalidConfigurationException(
                sprintf('"%s" must implement "%s"', $userRepositoryClass, UserRepositoryInterface::class)
            );
        }

        if (UserRepository::class === $userRepositoryClass) {
            $definition = new Definition($userRepositoryClass);
            $definition
                ->setAutowired(true)
                ->setAutoconfigured(true)
                ->setArgument('$userClass', $container->getParameter('tunet_user.user_class'))
            ;

            $container->setDefinition($userRepositoryClass, $definition);
        }

        $container->setAlias(UserRepositoryInterface::class, $userRepositoryClass);
    }
}
