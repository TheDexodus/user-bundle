<?php

declare(strict_types=1);

namespace Tunet\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Tunet\UserBundle\Repository\UserRepository;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('tunet_user');
        $treeBuilder
            ->getRootNode()
            ->children()
                ->scalarNode('user_class')->isRequired()->end()
                ->scalarNode('user_repository')->defaultValue(UserRepository::class)->end()
                ->scalarNode('login_route')->defaultValue('login')->end()
                ->scalarNode('home_route')->defaultValue('home')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
